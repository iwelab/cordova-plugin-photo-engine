//
//  PhotoEngine.m
//  PhotoEnginePlugin
//

#import "PhotoEngine.h"
#import <CoreLocation/CoreLocation.h>

static int count=0;
static int loop=0;

@implementation PhotoEngine

- (void)photoList:(CDVInvokedUrlCommand *)command
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    NSMutableArray *assetURLDictionaries = [[NSMutableArray alloc] init];
    keyList = [[NSMutableArray alloc]init];

    [keyList removeAllObjects];
    loop = 0;

    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];

                NSURL *url= (NSURL*) [[result defaultRepresentation]url];

                [library assetForURL:url resultBlock:^(ALAsset *asset) {
                    CLLocation *location = [asset valueForProperty:ALAssetPropertyLocation];

                    if (location.coordinate.latitude && location.coordinate.longitude) {
                      NSDate *date = [asset valueForProperty:ALAssetPropertyDate];

                      NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSNumber numberWithInt:(int)[keyList count]], @"id",
                        [NSString stringWithFormat:@"%@", url], @"url",
                        [NSString stringWithFormat:@"%f", [date timeIntervalSince1970]], @"timestamp",
                        [NSString stringWithFormat:@"%f", location.coordinate.latitude], @"latitude",
                        [NSString stringWithFormat:@"%f", location.coordinate.longitude], @"longitude",
                        nil];

                      [keyList addObject:dictionary];

                      NSLog(@"%@", dictionary);
                    }

                    if (++loop == count)
                    {
                        //NSError *error;
                        //NSData *jsonData = [NSJSONSerialization dataWithJSONObject:keyList options:0 error:&error];
                        //NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                        //[self sendPluginResult:command error:0 data:keyList];

                        [self.commandDelegate runInBackground:^{
                            NSDictionary *jsonDictionary = @{@"error": [NSNumber numberWithInt:0], @"data": keyList};
                            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary: jsonDictionary];
                            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                        }];
                    }
                }
                failureBlock:^(NSError *error){ NSLog(@"failureBlock: %@", error); } ];
            }
        }
    };

    NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
    assetGroups = [[NSMutableArray alloc] init];

    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            [group enumerateAssetsUsingBlock:assetEnumerator];
            [assetGroups addObject:group];
            count=(int)[group numberOfAssets];
        }
    };

    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {NSLog(@"There is an error");}];
}

- (void)storePhoto:(CDVInvokedUrlCommand *)command
{
    NSString *message = [command.arguments objectAtIndex:0];

    NSDictionary *dic = [keyList objectAtIndex:[message integerValue]];
    NSURL *url = [NSURL URLWithString:[[dic objectForKey:@"url"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
    [assetLibrary assetForURL:url resultBlock:^(ALAsset *asset) {
        UIImage *image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
        if (image == nil)
        {
            //[self sendPluginResult:command error:100 data:nil];

            [self.commandDelegate runInBackground:^{
                NSDictionary *jsonDictionary = @{@"error": [NSNumber numberWithInt:100], @"data": @""};
                CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary: jsonDictionary];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }];
        } else {
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];

            NSString *imageIndex = [NSString stringWithFormat:@"%ld", (long)[message integerValue]]; 
            NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:imageIndex];

            NSLog(@"image path = %@", imagePath);

            [self saveImageToSandbox:image path:imagePath];

            [self.commandDelegate runInBackground:^{
                NSDictionary *jsonDictionary = @{@"error": [NSNumber numberWithInt:0], @"data": imagePath};
                CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary: jsonDictionary];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }];
        }

    }
    failureBlock:^(NSError *error){
    } ];
}

-(void)saveImageToSandbox:(UIImage*)image path:(NSString *)path
{
    NSLog(@"sandbox path = %@", path);
    UIImage *resizedImage = [self resizeImage:image maxDimension:1280];
    [UIImageJPEGRepresentation(resizedImage, 0.7) writeToFile:path atomically:YES];
}

- (UIImage *)resizeImage:(UIImage *)image maxDimension:(CGFloat)maxDimension
{
    if (fmax(image.size.width, image.size.height) <= maxDimension) {
        return image;
    }

    CGFloat aspect = image.size.width / image.size.height;
    CGSize newSize;

    if (image.size.width > image.size.height) {
        newSize = CGSizeMake(maxDimension, maxDimension / aspect);
    } else {
        newSize = CGSizeMake(maxDimension * aspect, maxDimension);
    }

    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
    CGRect newImageRect = CGRectMake(0.0, 0.0, newSize.width, newSize.height);
    [image drawInRect:newImageRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

@end
