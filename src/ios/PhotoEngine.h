//
//  PhotoEngine.h
//  PhotoEnginePlugin
//

#import <Cordova/CDV.h>
#include <AssetsLibrary/AssetsLibrary.h>

@interface PhotoEngine : CDVPlugin
{
    NSMutableArray *keyList;
}

- (void)photoList:(CDVInvokedUrlCommand *)command;
- (void)storePhoto:(CDVInvokedUrlCommand *)command;
- (void)saveImageToSandbox:(UIImage*)image path:(NSString *)path;
- (UIImage *)resizeImage:(UIImage *)image maxDimension:(CGFloat)maxDimension;
//- (void)sendPluginResult:(CDVInvokedUrlCommand *)command error:(NSInteger)error data:(NSMutableArray *)data;
@end
